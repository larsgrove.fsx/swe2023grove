def encode(mess):
	""" Run length encoding - konverterer strenge fra 'wwwww' til '5w' """
	
	res = []
	old = mess[0]
	i = 0
	for c in mess:
		if c == old:
			i += 1
		else:
			res.append('%d%c' % (i,old))
			old = c
			i = 1
	res.append('%d%c' % (i,c))
	return ''.join(res)


def decode(mess):
	""" Run length decoding - decode compressed strings to multiple characters"""

	output = []
	num = 0
	for c in mess:
		if c.isdigit():
			num = 10*num + int(c)
			
		else:
			output.append(num*c)
			num = 0

	return ''.join(output)




def choose_func(mess):
	"""Determine whether input should be encoded or decoded"""

	foundnumber = 0
	for c in mess:
		if c.isnumeric():
			foundnumber = 1			
	if foundnumber==1:
		print(decode(mess))
		return(decode(mess))
	else:
		return(decode(mess))
		print(encode(mess))
#print("Please enter a string for encode/decode\n")
#stringinput = input()
#choose_func(stringinput)


if __name__ == '__main__':
    import sys
    argv = sys.argv
    if '-e' in argv:
        filename = argv[argv.index('-e')+1]
        func = encode # func er en funktion. Her er det encode
    elif '-d' in argv:
        filename = argv[argv.index('-d')+1]
        func = decode # func er en funktion. Her er det decode
    else:
        filename = argv[-1]
        func = lambda x: decode(encode(x)) # func er en funktion. Her decode(encode(x))
    with open(filename,'r',encoding="iso-8859-1") as f:
        print(func(f.read()))
    # Denne pause er nødvendig for at fuzzeren kan skelne
    # mellem crash og succes (en fejl i fuzzzeren)
    time.sleep(1)
    exit(0)
	
	
from rle import encode
from rle import decode
from rle import choose_func

def test_encode():
	assert encode('aa') == '2a'
	assert encode('wwwwwbbb') == '5w3b'
	assert decode(encode('bbbwwwww')) == 'bbbwwwww'

def test_decode():
	assert decode('2a5b') == 'aabbbbb'


def test_choose_func():
	assert choose_func("3w5y") == "wwwyyyyy"